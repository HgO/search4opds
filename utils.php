<?php
    /**
     * @licence GPL 3 (https://www.gnu.org/licenses/gpl-3.0.html)
     * @author HgO
     */
    
    class Utils {
        public static function plural($str, $n = 2) {
            if($n > 1)
                if(substr($str, -1) != 's')
                    return $str.'s';
            
            return $str;
        }
    }