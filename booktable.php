<?php
    /**
     * @licence GPL 3 (https://www.gnu.org/licenses/gpl-3.0.html)
     * @author HgO
     */
    
    class BookTable extends Table {
        const COMMENT_TABLE = "comments";
        
        public function __construct() {
            parent::__construct("books");
        }
        
        protected function searchQuery($query, &$binds, $operator) {
            $fields = array('title');
            $joins = array();
            
            if(Config::DEEP_SEARCH) {
                $joins[] = array(BookTable::COMMENT_TABLE, 'book', 'id');
                
                $fields[] = BookTable::COMMENT_TABLE.'.text';
            }
                    
            return $this->buildSearchQuery($query, $binds, $operator, $fields, $joins);
        }
        
        public function search($query) {
            return $this->executeSearch(array('id', 'title', 'path', 'has_cover'), $query, array('sort ASC'));
        }
        
        public function createDAO($row) {
            return new Book($row);
        }
    }