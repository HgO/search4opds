<?php
    /**
     * @licence GPL 3 (https://www.gnu.org/licenses/gpl-3.0.html)
     * @author HgO
     */
    
    class SerieTable extends MetadataTable {
        public function __construct() {
            parent::__construct("series");
        }
        
        public function createDAO($row) {
            return new Serie($row);
        }
    }