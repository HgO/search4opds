<?php
    /**
     * @licence GPL 3 (https://www.gnu.org/licenses/gpl-3.0.html)
     * @author HgO
     */
    
    abstract class MetadataTable extends Table {
        protected function searchQuery($query, &$binds, $operator) {
            return $this->buildSearchQuery($query, $binds, $operator, array('name'));
        }
        
        public function search($query) {
            return $this->executeSearch(array('id', 'name'), $query, array('sort ASC'));
        }
    }