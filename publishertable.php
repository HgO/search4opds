<?php
    /**
     * @licence GPL 3 (https://www.gnu.org/licenses/gpl-3.0.html)
     * @author HgO
     */
    
    class PublisherTable extends MetadataTable {
        public function __construct() {
            parent::__construct("publishers");
        }
        
        public function createDAO($row) {
            return new Publisher($row);
        }
    }