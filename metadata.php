<?php
    /**
     * @licence GPL 3 (https://www.gnu.org/licenses/gpl-3.0.html)
     * @author HgO
     */
    
    class Metadata extends DAO {
        protected $name;
        
        protected $books = array();
        
        public function name() {
            return $this->name;
        }
        
        public function addBook(Book $book) {
            $this->books[] = $book;
        }
    }
    