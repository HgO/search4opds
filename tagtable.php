<?php
    /**
     * @licence GPL 3 (https://www.gnu.org/licenses/gpl-3.0.html)
     * @author HgO
     */
    
    class TagTable extends MetadataTable {
        public function __construct() {
            parent::__construct("tags");
        }
        
        public function search($query) {
            return $this->executeSearch(array('id', 'name'), $query, array('name ASC'));
        }
        
        public function createDAO($row) {
            return new Tag($row);
        }
    }