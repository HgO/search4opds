<?php
    /**
     * @licence GPL 3 (https://www.gnu.org/licenses/gpl-3.0.html)
     * @author HgO
     */
    
    class Cache {
        private $filename;
        
        public function __construct($filename) {
            $this->filename = Config::CACHE_DIRECTORY.$filename;
        }
        
        public function exists() {
            return Config::CACHE and is_readable($this->filename);
        }
        
        public function write($content) {
            if(Config::CACHE) {
                if(!file_exists(Config::CACHE_DIRECTORY))
                    mkdir(CONFIG::CACHE_DIRECTORY, 0750, true);
                    
                file_put_contents($this->filename, $content);
            }
        }
        
        public function display() {
            require $this->filename;
        }
    }