<div id="calibre:book:<?php echo $book->id(); ?>" class="x_container">
    <div class="cover">
        <a href="<?php echo Config::CATALOG_DIRECTORY; ?>book_0/book_<?php echo $book->id(); ?>.html" title="<?php echo htmlentities($book->title()); ?>">
            <img src="<?php echo Config::CALIBRE_DIRECTORY.implode('/', array_map('rawurlencode', explode('/', $book->path()))); ?>/c2o_thumbnail.jpg" width="96" height="144">
        </a>
    </div>
</div>