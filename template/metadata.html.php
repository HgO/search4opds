<?php if(!empty($metadata)) { ?>
<div id="authors" class="buttonwrapper">
    <h2><?php echo count($metadata).' '.Utils::plural($type, count($metadata)); ?></h2>
    <div class="browseByCover">
        <?php 
            foreach($metadata as $metadatum) { 
                $href = Config::CATALOG_DIRECTORY.$type.'_0/'.$type.'_'.$metadatum->id().'_Page_1.html';
                $name = htmlentities($metadatum->name());
        ?>
        <div class="x_menulisting">
            <div class="cover">
                <a href="<?php echo $href; ?>" title="<?php echo $name; ?>">
                    <img src="<?php echo Config::CATALOG_DIRECTORY.Utils::plural($type); ?>.png" border="0" />
                </a>
            </div>
            <div class="details">
                <a href="<?php echo $href; ?>" title="<?php echo $name; ?>">
                    <?php echo $name; ?>
                </a>
                <br/><br/>
            </div>
        </div>
        <?php } ?>
    </div>
</div>
<?php } ?>