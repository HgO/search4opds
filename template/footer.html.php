            <HR>
        </div>

        <footer>
            <small>
                <br />calibre2opds v 3.5<br />
                <br />Generate OPDS and HTML catalogs from your Calibre ebooks database<br />The project''s wiki : &nbsp;<a
                    href="http://wiki.mobileread.com/wiki/Calibre2opds"
                    >http://wiki.mobileread.com/wiki/Calibre2opds</a>
                <br />The Calibre2Opds team :<ul>
                    <li>David Pierron - main programmer</li>
                    <li>Dave Walker - guru, features manager and tester extraordinaire</li>
                    <li>Farid Soussi - css and html guru</li>
                    <li>Douglas Steele - programmer</li>
                </ul>
                <br />Special thanks to Kb Sriram, who not only programmed Trook, an excellent and OPDS compatible <br />library manager for the Nook, but also was kind enough to donate a Nook !</small>
        </footer>
    </body>
</html>
