<?php 
    require "template/header.html.php";
    
    if(!empty($this->entry)) { 
?>
<div id="Results">
    <?php 
        if($this->cache->exists())
            $this->cache->display();
        else {
            ob_start(); 
    ?>
    <h2><?php 
            $n = 0;
            foreach($this->results as $result) 
                $n += count($result);
                
            echo $n.' '.Utils::plural('result', $n); 
    ?> for "<?php echo htmlentities($this->entry); ?>"</h2>
    <?php 
            foreach($this->results as $type => $metadata) {
                if($type == 'book') {
                    $books = $metadata;
                    require "template/books.html.php";
                } else
                    require "template/metadata.html.php";
            }
        
            $this->cache->write(ob_get_contents());
            
            ob_end_flush();
        }
    ?>
</div>
<?php 
    }
    
    require "template/footer.html.php";
?>