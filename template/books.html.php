<?php if(!empty($books)) { ?>
<div id="books" class="buttonwrapper">
    <h2><?php echo count($books); ?> book<?php if(count($books) > 1) echo 's'; ?></h2>
    <div class="browseByCover">
        <?php 
            foreach($books as $book) { 
                $title = htmlentities($book->title());
        ?>
        <div id="calibre:book:<?php echo $book->id(); ?>" class="x_container">
            <div class="cover">
                <a href="<?php echo Config::CATALOG_DIRECTORY; ?>book_0/book_<?php echo $book->id(); ?>.html" title="<?php echo $title; ?>">
                    <img src="<?php echo Config::CALIBRE_DIRECTORY.implode('/', array_map('rawurlencode', explode('/', $book->path()))); ?>/c2o_thumbnail.jpg" width="96" height="144">
                </a>
            </div>
        </div>
        <?php } ?>
    </div>
</div>
<?php } ?>