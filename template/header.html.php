<!DOCTYPE html>
<html>
    <head>
        <link href="<?php echo Config::CATALOG_DIRECTORY; ?>desktop.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo Config::CATALOG_DIRECTORY; ?>mobile.css" media="only screen and (max-device-width: 480px)"
            rel="stylesheet" type="text/css" />
        <link href="template/css/desktop.css" rel="stylesheet" type="text/css" />
        <title>Search</title>
    </head>
    <body style="">
        <header>
            <h1>Search library</h1>
        </header>
        
        <ul id="breadcrumb">
            <li>
                <a href="<?php echo Config::CATALOG_DIRECTORY; ?>/index.html" title="Main catalog">Main catalog</a>
            </li>
            <li><a class="notLast">Search</a></li>
            <li>
                <form method="get" action="search.php">
                    <input autofocus id="query" name="query" placeholder="Search KeyWords" type="search" />
                </form>
            </li>
        </ul>

        <div id="search">
            <p></p>