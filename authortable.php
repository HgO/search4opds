<?php
    /**
     * @licence GPL 3 (https://www.gnu.org/licenses/gpl-3.0.html)
     * @author HgO
     */
    
    class AuthorTable extends MetadataTable {
        public function __construct() {
            parent::__construct("authors");
        }
        
        public function createDAO($row) {
            return new Author($row);
        }
    }