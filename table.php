<?php
    /**
     * @licence GPL 3 (https://www.gnu.org/licenses/gpl-3.0.html)
     * @author HgO
     */
    
    abstract class Table {
        protected $db;
        
        protected $table;
        
        public function __construct($table) {
            $this->db = Database::instance();
            
            $this->table = $table;
        }
    
        public function count() {
            $query = 'SELECT COUNT(id) FROM '.$this->table;
            
            $result = $this->db->query($query);
            $row = $result->fetchArray(SQLITE3_NUM);
            $result->finalize();
            
            return $row[0];
        }
        
        protected abstract function searchQuery($query, &$binds, $operator);
        
        protected function buildSearchQuery($query, &$binds, $operator, $fields, $joins = array()) {
            $sql = 'SELECT s.id
                    FROM '.$this->table.' s ';
            
            foreach($joins as $join) {
                if(!strpos($join[1], '.'))
                    $join[1] = $join[0].'.'.$join[1];
                if(!strpos($join[2], '.'))
                    $join[2] = 's.'.$join[2];
            
                $sql .= 'LEFT JOIN '.$join[0].'
                            ON '.$join[1].' = '.$join[2].' ';
            }
            
            $sql .= 'WHERE ';
            
            for($i = 0; $i < count($fields); ++$i) {
                if(!strpos($fields[$i], '.'))
                    $fields[$i] = 's.'.$fields[$i];
                
                $fields[$i] .= ' LIKE ?';
            }
            
            for($i = 0; $i < count($query); ++$i) {
                if($i != 0)
                    $sql .= ' '.$operator.' ';
                
                $sql .= '(' . implode(' OR ', $fields) . ')';
                
                for($j = 0; $j < count($fields); ++$j)
                    $binds[] = $query[$i];
            }
            
            return $sql;
        }
        
        protected function searchIntersection($query, &$binds) {
            return $this->searchQuery($query, $binds, 'AND');
        }
        
        protected function searchUnion($query, &$binds) {
            return $this->searchQuery($query, $binds, 'OR');
        }
        
        public abstract function search($query);
        
        protected function executeSearch($fields, $query, $sort = null) {
            $binds = array();
            
            $sql = 'SELECT '.implode(', ', $fields).
                    ' FROM '.$this->table.'
                    WHERE ';
        
            $conditions = array();
            if(!empty($query->ands))
                $conditions[] = 'id IN (' . $this->searchIntersection($query->ands, $binds) . ')';
            if(!empty($query->ors))
                $conditions[] = 'id IN (' . $this->searchUnion($query->ors, $binds) . ')';
            if(!empty($query->nots))
                $conditions[] = 'id NOT IN (' . $this->searchUnion($query->nots, $binds) . ')';
            
            $sql .= implode(' AND ', $conditions);
            
            if(!is_null($sort))
                $sql .= 'ORDER BY '. implode(', ', $sort);
            
            $statement = $this->db->prepare($sql);
            
            for($i = 0; $i < count($binds); ++$i)
                $statement->bindValue($i+1, '%'.$binds[$i].'%', SQLITE3_TEXT);
            
            $result = $statement->execute();
            
            $array = array();
            while($row = $result->fetchArray(SQLITE3_ASSOC)) {
                $array[] = $this->createDAO($row);
            }
            
            $result->finalize();
            
            return $array;
        }
        
        public abstract function createDAO($row);
    }