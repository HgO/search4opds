<?php 
    /**
     * @licence GPL 3 (https://www.gnu.org/licenses/gpl-3.0.html)
     * @author HgO
     */
    
    class Config {
        const CALIBRE_DIRECTORY = 'ebooks/';
        const CATALOG_DIRECTORY = Config::CALIBRE_DIRECTORY.'_catalog/';
        const DATABASE = 'metadata.db';
        const LANGUAGE = 'fr';
        const CACHE = false;
        const CACHE_DIRECTORY = 'cache/';
        const DEEP_SEARCH = true;
    }