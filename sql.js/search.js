var Config = {
  HOST : 'search4opds.moriarty',
  DATABASE : 'metadata.db',
  DEEP_SEARCH : true
}

  
function DAO(row) {
  for(var field in row)
    this[field] = row[field];
}

function Book(row) {
  DAO.call(this, row);
}
Book.prototype = Object.create(DAO.prototype);
Book.prototype.constructor = DAO;

function Metadata(row) {
  DAO.call(this, row);
}
Metadata.prototype = Object.create(DAO.prototype);
Metadata.prototype.constructor = DAO;


function Table(table) {
  this.table = table;
  this.db = Database.instance();
}

Table.prototype.buildSearchQuery = function(query, binds, operator, fields, joins) {
  joins = (typeof joins === 'undefined') ? [] : joins;
  
  var sql = 'SELECT s.id FROM '+this.table+' s ';
  
  for(var i = 0; i < joins.length; ++i) {
    var join = joins[i];
    
    if(join[1].indexOf('.') == -1)
      join[1] = join[0]+'.'+join[1];
    if(join[2].indexOf('.') == -1)
      join[2] = 's.'+join[2];
    
    sql += 'LEFT JOIN '+join[0] +' ON '+join[1]+' = '+join[2]+' ';
  }
  
  sql += 'WHERE ';
  
  for(var i = 0; i < fields.length; ++i) {
    if(fields[i].indexOf('.') == -1)
      fields[i] = 's.'+fields[i];
    
    fields[i] += ' LIKE ?';
  }
  
  for(var i = 0; i < query.length; ++i) {
    if(i != 0)
      sql += ' '+operator+' ';
    
    sql += '('+fields.join(' OR ')+')';
    
    for(var j = 0; j < fields.length; ++j)
      binds.push('%' + query[i] + '%');
  }
  
  return sql;
}

Table.prototype.searchIntersection = function(query, binds) {
  return this.searchQuery(query, binds, 'AND');
}

Table.prototype.searchUnion = function(query, binds) {
  return this.searchQuery(query, binds, 'OR');
}

Table.prototype.executeSearch = function (fields, query, sort) {
  sort = (typeof sort === 'undefined') ? null : sort;
  
  var binds = [];
  var conditions = [];
  var sql = 'SELECT '+fields.join(', ')+' FROM '+this.table+' WHERE ';
  
  if(query.ands.length > 0)
    conditions.push('id IN (' + this.searchIntersection(query.ands, binds) + ')');
  if(query.ors.length > 0)
    conditions.push('id IN (' + this.searchUnion(query.ors, binds) + ')');
  if(query.nots.length > 0)
    conditions.push('id NOT IN (' + this.searchUnion(query.nots, binds) + ')');
  
  sql += conditions.join(' AND ');
  
  if(sort && sort.length > 0)
    sql += 'ORDER BY '+sort.join(', ');
  
  var statement = this.db.prepare(sql);
  statement.bind(binds);
  
  var results = [];
  while(statement.step()) {
    results.push(this.createDAO(statement.getAsObject()));
  }
  
  statement.free();
  return results;
}


function BookTable() {
  Table.call(this, 'books');
  
  this.COMMENT_TABLE = 'comments';
}
BookTable.prototype = Object.create(Table.prototype);
BookTable.prototype.constructor = Table;

BookTable.prototype.searchQuery = function(query, binds, operator) {
  var fields = ['title'];
  var joins = [];
  
  if(Config.DEEP_SEARCH) {
    joins.push([this.COMMENT_TABLE, 'book', 'id']);
    
    fields.push(this.COMMENT_TABLE + '.text');
  }
  
  return this.buildSearchQuery(query, binds, operator, fields, joins);
}

BookTable.prototype.search = function(query) {
  return this.executeSearch(['id', 'title', 'path'], query, ['sort ASC']);
}

BookTable.prototype.createDAO = function(row) {
  return new Book(row);
}


function MetadataTable(table) {
    Table.call(this, table);
}
MetadataTable.prototype = Object.create(Table.prototype);
MetadataTable.prototype.constructor = Table;

MetadataTable.prototype.searchQuery = function(query, binds, operator) {
  return this.buildSearchQuery(query, binds, operator, ['name']);
} 

MetadataTable.prototype.search = function(query) {
  return this.executeSearch(['id', 'name'], query, ['sort ASC']);
}

MetadataTable.prototype.createDAO = function(row) {
  return new Metadata(row);
}


function AuthorTable() {
  MetadataTable.call(this, 'authors');
}
AuthorTable.prototype = Object.create(MetadataTable.prototype);
AuthorTable.prototype.constructor = MetadataTable;

function TagTable() {
  MetadataTable.call(this, 'tags');
}
TagTable.prototype = Object.create(MetadataTable.prototype);
TagTable.prototype.constructor = MetadataTable;

TagTable.prototype.search = function(query) {
  return this.executeSearch(['id', 'name'], query, ['name ASC']);
}

function SerieTable() {
  MetadataTable.call(this, 'series');
}
SerieTable.prototype = Object.create(MetadataTable.prototype);
SerieTable.prototype.constructor = MetadataTable;

function PublisherTable() {
  MetadataTable.call(this, 'publishers');
}
PublisherTable.prototype = Object.create(MetadataTable.prototype);
PublisherTable.prototype.constructor = MetadataTable;


var State = {
  NONE : 0,
  DOUBLE_QUOTE : 1,
  BACKSLASH : 2
}

function Query() {
  this.ands = [];
  this.ors = [];
  this.nots = [];
}


function Search(entry) {
  this.entry = entry;
  this.tokens = [];
  this.query = null;
  
  this.tables = {
    'bbok' : new BookTable(),
    'author' : new AuthorTable(),
    'tag' : new TagTable(),
    'series' : new SerieTable(),
    //'publisher' : new PublisherTable()
  }
  
  this.results = {};
}

Search.prototype.tokenize = function() {
  this.tokens = [];
  var state = State.NONE;
  var start = 0, length = 0;
  var oldstate = State.NONE;
  
  for(var i = 0; i < this.entry.length; ++i, ++length) {
    var c = this.entry[i];
    if(c == '"' && state != State.BACKSLASH) {
      if(state == State.DOUBLE_QUOTE)
        state = State.NONE;
      else
        state = State.DOUBLE_QUOTE;
      
      if(length > 0)
        this.tokens.push(this.entry.substr(start, length));
      
      start = i+1;
      length = -1;
    } else {
      if(state == State.BACKSLASH)
        state = oldstate;
      
      if(c == ' ' && state == State.NONE) {
        if(length > 0)
          this.tokens.push(this.entry.substr(start, length));
        
        start = i+1;
        length = -1;
      } else if(c == '\\') {
        oldstate = state;
        state = State.BACKSLASH;
      }
    }
  }
  
  if(length > 0)
    this.tokens.push(this.entry.substr(start, length));
  
  
  for(var i = 0; i < this.tokens.length; ++i) {
    var token = this.tokens[i];
    token = token.replace('\\"', '"');
    token = token.trim();
    
    if(token)
      this.tokens[i] = token;
    else
      this.tokens.splice(i, 1);
  }
  
  for(var i = 0; i < this.tokens.length; ++i)
    if(this.tokens[i] == '+' || this.tokens[i] == '-') {
      this.tokens[i] += this.tokens[i+1];
      this.tokens.splice(i+1, 1);
    }
  
  return this.tokens;
}

Search.prototype.group = function() {
  this.query = new Query();
  
  for(var i = 0; i < this.tokens.length; ++i) {
    var token = this.tokens[i];
    
    switch(token[0]) {
      case '+':
        this.query.ands.push(token.substr(1, token.length));
        break;
      case '-':
        this.query.nots.push(token.substr(1, token.length));
        break;
      default:
        this.query.ors.push(token);
    }
  }
  
  return this.query;
}

Search.prototype.search = function() {
  if(this.entry) {
    this.tokenize();
    
    if(this.tokens.length > 0) {
      this.group();
      
      for(var key in this.tables) {
        this.results[key] = this.tables[key].search(this.query);
      }
    }
  }
  
  return this.results;
}


var xhr = new XMLHttpRequest();
xhr.open('GET', 'http://' + Config.HOST + '/' + Config.DATABASE);
xhr.responseType = 'arraybuffer';

var Database = (function() {
  var db = null;
  
  return { 
    instance : function() {
      if(!db) {
        var uInt8Array = new Uint8Array(xhr.response);
        db = new SQL.Database(uInt8Array);
      }
      
      return db;
    }
  };
})();

xhr.onload = function(e) {
  var entry = "test";
  var search = new Search(entry);
  var results = search.search();
    
  Database.instance().close();
  
  for(var key in results)
    for(var row = 0; row < results[key].length; ++row)
      if(results[key][row] instanceof Book)
        alert(results[key][row].title);
      else
        alert(results[key][row].name);
}

xhr.send();
