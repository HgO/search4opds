<?php
    /**
     * @licence GPL 3 (https://www.gnu.org/licenses/gpl-3.0.html)
     * @author HgO
     */
    
    class Book extends DAO {
        protected $title;
        protected $series_index;
        protected $path;
        protected $has_cover;
        protected $comment;
        protected $rating;
        
        private $authors = array();
        private $series = array();
        private $tags = array();
        private $publishers = array();
        private $languages = array();
        private $data = array();
        
        public function title() {
            return $this->title;
        }
        
        public function series_index() {
            return $this->series_index;
        }
        
        public function path() {
            return $this->path;
        }
        
        public function has_cover() {
            return $this->has_cover;
        }
        
        public function comment() {
            return $this->comment;
        }
        
        public function rating() {
            return $this->rating;
        }
        
        public function addAuthor(Author $author) {
            $this->authors[] = $author;
        }
        
        public function authors() {
            return $this->authors;
        }
        
        public function addSerie(Serie $serie) {
            $this->series[] = $serie;
        }
        
        public function series() {
            return $this->series;
        }
        
        public function addTag(Tag $tag) {
            $this->tags[] = $tag;
        }
        
        public function tags() {
            return $this->tags;
        }
        
        public function addLanguage(Language $language) {
            $this->languages[] = $language;
        }
        
        public function languages() {
            return $this->languages;
        }
        
        public function addPublisher(Publisher $publisher) {
            $this->publishers[] = $publisher;
        }
        
        public function publishers() {
            return $this->publishers;
        }
        
        public function addData(Data $data) {
            $this->data[] = $data;
        }
        
        public function data() {
            return $this->data;
        }
    }
    