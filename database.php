<?php
    /**
     * @licence GPL 3 (https://www.gnu.org/licenses/gpl-3.0.html)
     * @author HgO
     */
    
    class Database {
        protected function __construct() { }
        
        static public function instance() {
            static $instance = null;
            
            if(is_null($instance))
                $instance = new Sqlite3(Config::DATABASE, SQLITE3_OPEN_READONLY);
            
            return $instance;
        }
    }