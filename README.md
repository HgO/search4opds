## Introduction

search4opds is a small php search engine for [calibre2opds](http://calibre2opds.com/), since this latter doesn't have a fully compatible search engine (see [this forum](https://getsatisfaction.com/calibre2opds/topics/search_possibility)).

The main purpose of search4opds is to be installed on a PirateBox, i.e. a small router, which is why it has to be both fast and light. 

Note that a possible alternative may be to use [sql.js](https://github.com/kripken/sql.js). You'll find a javascript implementation of the search engine using sql.js in the directory `sql.js`. This implementation still need some compatibility tests on different smartphones and browsers (for now, it has been tested on Firefox 37 and Chrome 42)

## Installation

First, you need to generate your Calibre library with [calibre2opds](http://calibre2opds.com/), with the search option enabled.

Then, in the catalog's home page, replace this text `_search/search.html` with `/path/to/search.php`, in order to link the catalog with search4opds. 

For example, you can run this command if the catalog directory is `ebooks/_catalog/`:

```bash
sed -i 's/_search\/search.html/..\/..\/search.php/g' _catalog/index.html
```
Finally, copy the Calibre database (a file called `metadata.db`) in the project directory.

You'll need to reproduce these steps each time you want to regenerate your catalog with calibre2opds.

## Configuration

The settings are defined in `config.php`. 

For instance, you can enable the cache (experimental) or enable the deep search (i.e. searching into book's description).

## Usage

By default, the search engine make a search on any of the words provided. 

You can prefix a word with `+` to make it required, or with `-` to remove results containing this word. You can also use double quotes.