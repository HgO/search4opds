<?php
    /**
     * @licence GPL 3 (https://www.gnu.org/licenses/gpl-3.0.html)
     * @author HgO
     */
    
    class DAO {
        protected $id;
        
        public function __construct($row) {
            foreach($row as $column => $value) {
                if(property_exists($this, $column))
                    $this->{$column} = $value;
            }
        }
        
        public function id() {
            return $this->id;
        }
    }
    