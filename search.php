<?php
    /**
     * @licence GPL 3 (https://www.gnu.org/licenses/gpl-3.0.html)
     * @author HgO
     */
    
    function __autoload($class) {
        require_once(strtolower($class).".php");
    }
    
    
    class State {
        const NONE = 0;
        const DOUBLE_QUOTE = 1;
        const BACKSLASH = 2;
    }

    class Query {
        public $ands = array();
        public $ors = array();
        public $nots = array();
    }
    
    
    class Search {
        private $entry;
        private $query;
        private $tokens;
        
        private $cache;
        
        private $results;
        private $tables;
        
        public function __construct($entry = '') {
            $this->entry = $entry;
            
            $this->tables['book'] = new BookTable();
            $this->tables['author'] = new AuthorTable();
            $this->tables['tag'] = new TagTable();
            $this->tables['series'] = new SerieTable();
            //FIXME: calibre2opds dosn't generate publishers list 
            //$this->tables['publisher'] = new PublisherTable();
            
            $this->results = array();
            
            $this->cache = new Cache(urlencode($this->entry).'.html');
        }
        
        public function search() {
            if(!empty($this->entry) and !$this->cache->exists()) {
                $this->tokenize();
                
                if(!empty($this->tokens)) {
                    $this->group();
                    
                    foreach($this->tables as $type => $table)
                        $this->results[$type] = $table->search($this->query);
                }
            }
        }
        
        protected function group() {
            $this->query = new Query();
            
            foreach($this->tokens as $token) {
                switch($token[0]) {
                    case '+':
                        $this->query->ands[] = substr($token, 1, strlen($token));
                        break;
                    case '-':
                        $this->query->nots[] = substr($token, 1, strlen($token));
                        break;
                    default:
                        $this->query->ors[] = $token;
                }
            }
            
            return $this->query;
        }
        
        protected function tokenize() {
            $n = strlen($this->entry);
            
            $this->tokens = array();
            $state = State::NONE;
            
            $start = 0;
            $length = 0;
            for($i = 0; $i < $n; ++$i, ++$length) {
                $char = $this->entry[$i];
                
                if($char == '"' and $state != State::BACKSLASH) {
                    if($state == State::DOUBLE_QUOTE)
                        $state = State::NONE;
                    else
                        $state = State::DOUBLE_QUOTE;
                        
                    if($length > 0)
                        $this->tokens[] = substr($this->entry, $start, $length);
                    
                    $start = $i+1;
                    $length = -1;
                } else {
                    if($state == State::BACKSLASH)
                        $state = $oldstate;
                    
                    if($char == ' ' and $state == State::NONE) {
                        if($length > 0)
                            $this->tokens[] = substr($this->entry, $start, $length);
                                
                        $start = $i+1;
                        $length = -1;
                    } elseif($char == '\\') {
                        $oldstate = $state;
                        $state = State::BACKSLASH;
                    }
                }
            }
            
            if($length > 0)
                $this->tokens[] = substr($this->entry, $start, $length);
            
            
            /* Post-processing */
            
            $this->tokens = str_replace('\\"', '"', $this->tokens);
            
            $n = count($this->tokens);
            for($i = $n-1; $i >= 0; --$i)
                if($this->tokens[$i] == '+' or $this->tokens[$i] == '-') {
                    $this->tokens[$i] .= $this->tokens[$i+1];
                    unset($this->tokens[$i+1]);
                }
            
            $this->tokens = array_map('trim', $this->tokens);
            $this->tokens = array_filter($this->tokens);
            $this->tokens = array_unique($this->tokens);
            
            return $this->tokens;
        }
        
        public function render() {
            require "template/search.html.php";
        }
    }
    
    $query = (isset($_GET['query']) and !empty($_GET['query'])) ? trim($_GET['query']) : '';
    
    $search = new Search($query);
    $search->search();
    $search->render();